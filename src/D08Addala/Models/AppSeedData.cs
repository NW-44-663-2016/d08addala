﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Collections.Generic;

namespace D08Addala.Models
{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<ApplicationDbContext>();

            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            
            context.FootballStadiums.RemoveRange(context.FootballStadiums);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedStadiumsFromCsv(relPath, context);
        }

        private static void SeedStadiumsFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "footballStadium.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            FootballStadium.ReadAllFromCSV(source);
            List<FootballStadium> lst = FootballStadium.ReadAllFromCSV(source);
            context.FootballStadiums.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, ApplicationDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}
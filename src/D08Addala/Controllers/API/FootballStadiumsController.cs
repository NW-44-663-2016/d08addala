using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using D08Addala.Models;

namespace D08Addala.Controllers
{
    [Produces("application/json")]
    [Route("api/FootballStadiums")]
    public class FootballStadiumsController : Controller
    {
        private ApplicationDbContext _context;

        public FootballStadiumsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/FootballStadiums
        [HttpGet]
        public IEnumerable<FootballStadium> GetFootballStadiums()
        {
            return _context.FootballStadiums;
        }

        // GET: api/FootballStadiums/5
        [HttpGet("{id}", Name = "GetFootballStadium")]
        public IActionResult GetFootballStadium([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            FootballStadium footballStadium = _context.FootballStadiums.Single(m => m.StadiumID == id);

            if (footballStadium == null)
            {
                return HttpNotFound();
            }

            return Ok(footballStadium);
        }

        // PUT: api/FootballStadiums/5
        [HttpPut("{id}")]
        public IActionResult PutFootballStadium(int id, [FromBody] FootballStadium footballStadium)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != footballStadium.StadiumID)
            {
                return HttpBadRequest();
            }

            _context.Entry(footballStadium).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FootballStadiumExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/FootballStadiums
        [HttpPost]
        public IActionResult PostFootballStadium([FromBody] FootballStadium footballStadium)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.FootballStadiums.Add(footballStadium);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (FootballStadiumExists(footballStadium.StadiumID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetFootballStadium", new { id = footballStadium.StadiumID }, footballStadium);
        }

        // DELETE: api/FootballStadiums/5
        [HttpDelete("{id}")]
        public IActionResult DeleteFootballStadium(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            FootballStadium footballStadium = _context.FootballStadiums.Single(m => m.StadiumID == id);
            if (footballStadium == null)
            {
                return HttpNotFound();
            }

            _context.FootballStadiums.Remove(footballStadium);
            _context.SaveChanges();

            return Ok(footballStadium);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FootballStadiumExists(int id)
        {
            return _context.FootballStadiums.Count(e => e.StadiumID == id) > 0;
        }
    }
}